# Tomcat on Containers QuickStart

This repository contains artifacts to help you get started running Tomcat applications on Openshift.

## Getting started

We provide a WAR build of [Spring Pet Clinic](https://github.com/spring-petclinic/spring-framework-petclinic) in ROOT.war as a sample application. By following the steps below, you can see this QuickStart in action without the need to provide your own applciation or to make any modifications.

### Pre-requisites

For running in Openshift:

    GitLab Runner installed to OCP Bastion Host (environment from which you can run oc commands from)
    OCP Kubeconfig

### Building and testing locally

1. Clone the repository and navigate into the root of the repository:

    ```bash
    git clone https://gitlab.com/gitlab-partners-public/red-hat-demos/tomcat-container-quickstart.git
    cd tomcat-container-quickstart
    ```

1. Build the docker image:

    ```bash
    oc new-build --binary --name=tomcat
    oc start-build tomcat --from-dir=.

    ```

1. Run the image:

    ```bash
    oc new-app --name=${ENVIRONMENT_PREFIX}-$CI_COMMIT_SHORT_SHA --image=image-registry.openshift-image-registry.svc:5000/default/tomcat
    oc expose service/${ENVIRONMENT_PREFIX}-$CI_COMMIT_SHORT_SHA
    ```

